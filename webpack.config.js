const Encore = require('@symfony/webpack-encore');

// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

Encore
    .setOutputPath('public/build/app/')
    .setPublicPath('/build/app')
    .addEntry('app', './assets/app.js')
    .enableStimulusBridge('./assets/controllers.json')
    // .addStyleEntry('global', './assets/styles/app.scss')
    .enableSassLoader()
    .enableSourceMaps(!Encore.isProduction()).disableSingleRuntimeChunk()
    .splitEntryChunks()
    .autoProvidejQuery()
;

// build the first configuration
const app = Encore.getWebpackConfig();

// Set a unique name for the config (needed later!)
app.name = 'app';

// export the final configuration as an array of multiple configurations
module.exports = [app];
