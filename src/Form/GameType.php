<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GameType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('numberCard', IntegerType::class, array(
                'label' => 'Choisissez le nombre de carte',
                'data' => 5, //default value
                'constraints' => array(
                    new Assert\NotBlank(array('message' => 'La valeur ne doit pas être vide')),
                    new Assert\Type('integer'),
                    new Assert\GreaterThanOrEqual(array('value' => 1, 'message' => 'La valeur doit être supérieure ou égale à {{ compared_value }}.')),
                    new Assert\LessThanOrEqual(array('value' => 52, 'message' => 'La valeur doit être inférieure ou égale à {{ compared_value }}.'))
                )
            ))
            ->add('colors', HiddenType::class, array(
                'empty_data' => json_encode($options['colors'])
            ))
            ->add('values', HiddenType::class, array(
                'empty_data' => json_encode($options['values'])
            ))
            ->add('submit', SubmitType::class, array(
                'label' => 'Charger un jeu',
                'attr' => array(
                    'class' => 'btn btn-primary'
                )
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'colors' => null,
            'values' => null
        ]);
    }
}
