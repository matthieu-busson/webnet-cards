<?php

namespace App\Service\Helper;

use Symfony\Component\Form\FormInterface;

class HelperForm
{

    // Generate an array contains a key -> value with the errors where the key is the name of the form field
    public function getErrorMessages(FormInterface $form)
    {
        $errors = array();

        foreach ($form->getErrors() as $key => $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $child) {
            if (!$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
