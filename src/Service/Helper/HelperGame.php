<?php

namespace App\Service\Helper;

class HelperGame
{

    public function reduceArray(array $array): array
    {
        return array_reduce($array, 'array_merge', array());
    }

    public function generateGame(array $values, array $colors): array
    {
        $game = [];
        foreach ($colors as $color) {
            foreach ($values as $key => $value) {
                if(!isset($game[$color])) {
                    $game[$color] = [];
                }
                array_push($game[$color], ['value' => $value, 'color' => $color, 'level' => $key]);
            }
        }
        $game = $this->reduceArray($game);

        shuffle( $game);

        return $game;
    }

    public function getUnorderedHand(array $game, int $numberCard): array
    {
        return array_slice($game, 0,$numberCard, true);
    }

    public function getOrderedHand(array $cardsColors, array $handUnordered): array
    {
        $handOrdered = [];

        foreach ($cardsColors as $key => &$color) {
            foreach($handUnordered as $hand) {
                if ($color === $hand['color']){
                    if(!isset($handOrdered[$hand['color']])) {
                        $handOrdered[$hand['color']] = [];
                    }
                    array_push($handOrdered[$hand['color']], $hand);
                }
            }
        }

        foreach ($handOrdered as $key => &$color) {
            uasort($color, function($a,$b){
                return ($a["level"] <= $b["level"]) ? -1 : 1;
            });
        }

        return $this->reduceArray($handOrdered);
    }
}
