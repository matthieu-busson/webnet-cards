<?php

namespace App\Controller;

use App\Form\GameType;
use App\Service\Helper\HelperForm;
use App\Service\Helper\HelperGame;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GameController extends AbstractController
{
    #[Route('/', name: 'app_game_index')]
    public function index(Request $request ): Response
    {
        // colors by default order
        $colors = $this->getParameter('colors');
        // values by default order
        $values = $this->getParameter('values');

        $form = $this->createForm(GameType::class, null, ['colors' => $colors, 'values' => $values]);

        $form->handleRequest($request);

        return $this->renderForm('game/index.html.twig', [
            'form' => $form,
            'default_colors' => $colors,
            'default_values' => $values,
            'controller_name' => 'GameController'
        ]);
    }

    #[Route('/start', name: 'app_game_start', options: ['expose' => true])]
    public function startGameAjax(Request $request, HelperGame $helperGame, HelperForm $helperForm ): JsonResponse
    {
        // colors by default order
        $colors = $this->getParameter('colors');
        // values by default order
        $values = $this->getParameter('values');

        $form = $this->createForm(GameType::class, null, ['colors' => $colors, 'values' => $values]);
        $form->handleRequest($request);

        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array(
                'status' => 'Error',
                'message' => 'Error'),
                400);
        }

        if (!$form->isValid()) {
            return new JsonResponse(array(
                'status' => 'form_error',
                'message' => 'Invalid form',
                'data' => $helperForm->getErrorMessages($form)
            ));
        }

        $game = $request->request->get('game');
        // colors by default order
        $cardsColors = json_decode($game['colors']);
        // values by default order
        $cardsValues = json_decode($game['values']);
        // number of cards
        $numberCard = $game['numberCard'];

        $game = $helperGame->generateGame($cardsValues, $cardsColors);
        $handUnordered = $helperGame->getUnorderedHand($game, $numberCard);

        return new JsonResponse(array(
                'data' => [
                    'handUnordered' => $handUnordered,
                    'handOrdered' => $helperGame->getOrderedHand($cardsColors, $handUnordered)
                ],
                'status' => 'success',
                'code' => 200)
        );
    }
}
