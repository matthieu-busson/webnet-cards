#!/bin/sh
set -e
composer install --prefer-dist --no-progress --no-interaction
bin/console fos:js-routing:dump --format=json --target=public/js/fos_js_routes.json
yarn install
yarn encore prod
apache2-foreground