/*!
* Start Bootstrap - Agency v7.0.4 (https://startbootstrap.com/theme/agency)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-agency/blob/master/LICENSE)
*/
//
// Scripts
// 
import './styles/app.scss';
import {} from 'jquery-ui/ui/widgets/sortable';
import $ from 'jquery';
const routes = require('../public/js/fos_js_routes.json');
import Routing from '../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
import { Tooltip, Toast, Popover, ScrollSpy, Dropdown, Modal  } from 'bootstrap';
// start the Stimulus applications
import './bootstrap';

Routing.setRoutingData(routes)

$(document).ready(function() {

   $('#sortable_colors').sortable()
   $('#sortable_values').sortable()

   $('#game_submit').on('click', function(e){
      e.preventDefault(); // avoid to execute the actual submit of the form.

      hideElement('#game');
      showElement('#spinner');


      hideElement("#handOrderedWrapper"); // hide by default to reset display
      hideElement("#handUnorderedWrapper"); // hide by default to reset display

      var colors = [];
      var values = [];

      $('#sortable_values').children('li').each(function (i, elem) {
         values.push($(elem).text());
      });
      $('#sortable_colors').children('li').each(function(i, elem) {
         colors.push($(elem).text());
      });

      $('#game_colors').val( JSON.stringify( colors ) )
      $('#game_values').val( JSON.stringify( values ) )

      var form = $('form[name="game"]');

      $.ajax({
         type: "POST",
         url: Routing.generate('app_game_start'),
         data: form.serialize(), // serializes the form's elements.
         success: function(response)
         {
            $('#handUnordered').empty();
            $('#handOrdered').empty();
            $('.error-form').remove()

            if(response.status === 'form_error') {
               for (var key in response.data) {
                  $(form.find('[name*="'+key+'"]')[0]).before('<div class="alert alert-danger error-form"><strong>' + response.data[key] + '</strong></div>');
               }
            } else {
               $.each(response.data['handUnordered'], function( index, card ) {
                  $('#handUnordered').append('<li class="list-group-item">' + card.value + ' - ' + card.color + '</li>')
               });
               $.each(response.data['handOrdered'], function( index, card ) {
                  $('#handOrdered').append('<li class="list-group-item">' + card.value + ' - ' + card.color + '</li>')
               });
               showElement("#handUnorderedWrapper");
            }

            showElement('#game');
         },
         complete: function() {
           hideElement('#spinner');
         }
      });
   });

   $("#showOrderedHand").on('click', function(){
      showElement("#handOrderedWrapper")
   })
});

function hideElement(element) {
   $(element).addClass('d-none');
}

function showElement(element) {
   $(element).removeClass('d-none');
}

